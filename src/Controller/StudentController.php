<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Student;
use App\Form\StudentType;
use App\Repository\StudentRepository;
use Doctrine\Common\Persistence\ObjectManager;

class StudentController extends AbstractController
{

    /**
     * @Route("/student", name="student", methods="GET")
     */
    public function index(StudentRepository $studentRepository): Response
    {
        $students = $studentRepository->findAll();
        return $this->render('student/StudentList.html.twig', [
            'controller_name' => 'StudentController',
            'students' => $students
        ]);
    }
    /**
     * @Route("/student/new", name="student_new")
     */
    public function createStudent(Request $request, ObjectManager $objectManager): Response
    {
        $student = new Student();

        $form = $this->createForm(StudentType::class, $student);

        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $departements = $student->getDepartement();
            if ($departements != null and count($departements->getStudents()) >= $departements->getCapacity()) {
                return $this->redirectToRoute('student_new', ['error' => 'true']);
            }
            $objectManager->persist($student);
            $objectManager->flush();
            return $this->redirectToRoute('student_view', ['id' => $student->getId()]);
        }

        return $this->render('student/StudentForm.html.twig', ['formStudent' => $form->createView(), 'editMode' => 'false']);
    }

    /**
     * @Route("/student/{id}/edit", name="student_edit")
     */
    public function editStudent(Request $request, $id, StudentRepository $studentRepository, ObjectManager $objectManager): Response
    {
        $student = $studentRepository->find($id);

        $form = $this->createForm(StudentType::class, $student);

        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $objectManager->persist($student);
            $objectManager->flush();
            return $this->redirectToRoute('student_view', ['id' => $student->getId()]);
        }

        return $this->render('student/StudentForm.html.twig', ['formStudent' => $form->createView(), 'editMode' => 'true']);
    }

    /**
     * @Route("/student/{id}", name="student_view")
     */
    public function showStudent(StudentRepository $studentRepository, $id): Response
    {
        $student =  $studentRepository->find($id);
        if ($student == null) {
            return $this->render('student/Error.html.twig', ['message' => 'Etudiant Incorrect']);
        }
        return $this->render('student/StudentView.html.twig', ['student' => $student]);
    }

    /**
     * @Route("/student/{id}/remove", name="student_delete")
     */
    public function deleteStudent(StudentRepository $studentRepository, $id, ObjectManager $objectManager): Response
    {
        $student =  $studentRepository->find($id);
        if ($student == null) {
            return $this->render('student/Error.html.twig', ['message' => 'Erreur de Suppression']);
        }
        $objectManager->remove($student);
        $objectManager->flush();
        return $this->redirectToRoute('student');
    }
}
