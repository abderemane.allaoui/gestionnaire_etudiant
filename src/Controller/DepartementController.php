<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Departement;
use App\Repository\DepartementRepository;
use  App\Form\DepartementType;

class DepartementController extends AbstractController
{
    /**
     * @Route("/departement", name="departement")
     */
    public function index(DepartementRepository $departementRepository): Response
    {
        $departements = $departementRepository->findAll();
        return $this->render('departement/DepartementList.html.twig', [
            'controller_name' => 'DepartementController',
            'departements' => $departements
        ]);
    }

    /**
     * @Route("/departement/new", name="departement_new")
     */
    public function createDepartement(Request $request, ObjectManager $objectManager): Response
    {
        $departement = new Departement();

        $form = $this->createForm(DepartementType::class, $departement);

        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $objectManager->persist($departement);
            $objectManager->flush();
            return $this->redirectToRoute('departement_view', ['id' => $departement->getId()]);
        }

        return $this->render('departement/DepartementForm.html.twig', ['formDepartement' => $form->createView(), 'editMode' => 'false']);
    }

    /**
     * @Route("/departement/{id}/edit", name="departement_edit")
     */
    public function editDepartement(Request $request, $id, DepartementRepository $departementRepository, ObjectManager $objectManager): Response
    {
        $departement = $departementRepository->find($id);

        $form = $this->createForm(DepartementType::class, $departement);

        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $objectManager->persist($departement);
            $objectManager->flush();
            return $this->redirectToRoute('departement_view', ['id' => $departement->getId()]);
        }

        return $this->render('departement/DepartementForm.html.twig', ['formDepartement' => $form->createView(), 'editMode' => 'true']);
    }

    /**
     * @Route("/departement/{id}", name="departement_view")
     */
    public function showDepartement(DepartementRepository $departementRepository, $id): Response
    {
        $departement =  $departementRepository->find($id);
        if ($departement == null) {
            return $this->render('departement/Error.html.twig', ['message' => 'Departement Incorrect']);
        }
        return $this->render('departement/DepartementView.html.twig', ['departement' => $departement]);
    }

    /**
     * @Route("/departement/{id}/remove", name="departement_delete")
     */
    public function deleteDepartement(DepartementRepository $departementRepository, $id, ObjectManager $objectManager): Response
    {

        $departement =  $departementRepository->find($id);
        if ($departement == null) {
            return $this->render('departement/Error.html.twig', ['message' => 'Erreur de Suppression']);
        }
        $students = $departement->getStudents();
        foreach ($students as $student) {
            $student->setDepartement(NULL);
        }
        $objectManager->remove($departement);
        $objectManager->flush();
        return $this->redirectToRoute('departement');
    }
}
