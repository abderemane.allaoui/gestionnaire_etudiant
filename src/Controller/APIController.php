<?php

namespace App\Controller;

use App\Entity\Student;
use App\Entity\Departement;
use App\Repository\DepartementRepository;
use App\Repository\StudentRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

use Doctrine\Common\Collections\ArrayCollection;

class APIController extends AbstractController
{


    /**
     * @Route("/", name="accueil")
     */
    public function acceuil(): Response
    {
        return $this->render('Acceuil.html.twig', [
            'controller_name' => 'StudentController',
        ]);
    }

    /**
     * @Route("api/student", name="api_get_all_student", methods="GET")
     * 
     * @SWG\Response(
     *     response=200,
     *     description="Retourne tous les étudiant",
     *     @SWG\Schema(ref=@Model(type=Student::class))
     *     )
     * )
     * 
     */
    public function getAllStudent(StudentRepository $studentRepository): Response
    {
        $students = $studentRepository->findAll();

        $encoder = new JsonEncoder();
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {

                return $object->getId();
            },
        ];
        $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

        $serializer = new Serializer([$normalizer], [$encoder]);
        $jsonStudent = $serializer->serialize($students, 'json');
        $response = new Response($jsonStudent, 200, ["Content-Type" => "application/json"]);
        return $response;
    }
    /**
     * @Route("api/student", name="api_post_student", Methods="POST")
     * 
     * @SWG\Response(
     *     response=200,
     *     description="Ajoute un étudiant",
     *     @SWG\Schema(ref=@Model(type=Student::class))
     *     )
     * )
     * 
     * @SWG\Parameter(
     *     name="order",
     *     in="body",
     *     @SWG\Schema(ref=@Model(type=Student::class))
     * )
     * 
     */
    public function addStudent(Request $request, ValidatorInterface $validator): Response
    {
        $jsonStudent = $request->getContent();

        try {
            $student = $this->get('serializer')->deserialize($jsonStudent, Student::class, 'json');
            if ($student != null) {
                $manager = $this->getDoctrine()->getManager();
                $departements = $student->getDepartement();
                if ($departements != null and count($departements->getStudents()) >= $departements->getCapacity()) {
                    return $this->json(['message' => 'Capacité atteint'], 400);
                }

                $errors = $validator->validate($student);
                if (count($errors) > 0) {
                    return new Response($errors, 400, ["Content-Type" => "application/json"]);
                }
                $manager->persist($student);
                $manager->flush();
                return new Response($jsonStudent, 201, ["Content-Type" => "application/json"]);
            }
            return $this->json(['message' => 'Impossible d\'ajouter'], 400);
        } catch (NotEncodableValueException $e) {
            return $this->json(['message' => $e->getMessage()], 400, ["Content-Type" => "application/json"]);
        }
    }

    /**
     * @Route("api/student/{id}", name="api_put_student", Methods="PUT")
     * 
     * @SWG\Response(
     *     response=200,
     *     description="Ajoute un étudiant",
     *     @SWG\Schema(ref=@Model(type=Student::class))
     *     )
     * )
     * 
     * @SWG\Parameter(
     *     name="order",
     *     in="body",
     *     @SWG\Schema(ref=@Model(type=Student::class))
     * )
     * 
     */
    public function updateStudent(Request $request, $id, ValidatorInterface $validator, StudentRepository $studentRepository): Response
    {
        $jsonStudent = $request->getContent();

        try {
            $student = $this->get('serializer')->deserialize($jsonStudent, Student::class, 'json');
            $student_in_base = $studentRepository->find($id);

            if ($student != null and $student_in_base != null) {
                if ($student->getFirstName() != null)
                    $student_in_base->setFirstName($student->getFirstName());
                if ($student->getLastName() != null)
                    $student_in_base->setLastName($student->getLastName());
                if ($student->getNumEtud() != null)
                    $student_in_base->setNumEtud($student->getNumEtud());

                $manager = $this->getDoctrine()->getManager();
                $errors = $validator->validate($student_in_base);
                if (count($errors) > 0) {
                    return $this->json($errors, 400, ["Content-Type" => "application/json"]);
                }
                $manager->persist($student_in_base);
                $manager->flush();
                return new Response($jsonStudent, 201, ["Content-Type" => "application/json"]);
            }
            return $this->json(['message' => 'Impossible de modifer cette étudiant, vérifier bien s\'il existe ou si les données sont valide'], 400);
        } catch (NotEncodableValueException $e) {
            return $this->json(['message' => $e->getMessage()], 400, ["Content-Type" => "application/json"]);
        }
    }


    /**
     * @Route("api/student/{id}", name="api_student_view", Methods="GET")
     * 
     * @SWG\Response(
     *     response=200,
     *     description="Retourne un étudiant",
     *     @SWG\Schema(ref=@Model(type=Student::class))
     * )
     * 
     */
    public function showStudent(StudentRepository $studentRepository, $id): Response
    {
        $student =  $studentRepository->find($id);
        if ($student != null) {

            $encoder = new JsonEncoder();
            $defaultContext = [
                AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {

                    return $object->getId();
                },
            ];
            $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

            $serializer = new Serializer([$normalizer], [$encoder]);
            $jsonStudent = $serializer->serialize($student, 'json');
            $response = new Response($jsonStudent, 200, ["Content-Type" => "application/json"]);

            return $response;
        }
        return $this->json(['message' => 'Impossible d\'afficher l\'étudiant'], 400);
    }


    /**
     * @Route("api/student/{id}", name="api_delete_student", Methods="DELETE")
     * 
     * @SWG\Response(
     *     response=200,
     *     description="Supprime l'étudiant",
     *     @SWG\Schema(ref=@Model(type=Student::class))
     *     )
     * )
     * 
     */
    public function deleteStudent($id, StudentRepository $studentRepository): Response
    {
        $student =  $studentRepository->find($id);
        if ($student != null) {
            $manager = $this->getDoctrine()->getManager();
            $manager->remove($student);
            $manager->flush();
            return $this->json(['message' => 'Etudiant supprimé'], 200, ["Content-Type" => "application/json"]);
        }
        return $this->json(['message' => 'Impossible de supprimer l\'étudiant'], 400);
    }


    /**
     * @Route("/api/departement", name="api_get_all_departement", Methods="GET")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Retourne Tous les departements avec leurs étudiant",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Departement::class))
     *     )
     * )
     * 
     * 
     */
    public function getAllDepartement(DepartementRepository $departementRepository, SerializerInterface $serializer)
    {
        $departement = $departementRepository->findAll();

        $encoder = new JsonEncoder();
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
        ];
        $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

        $serializer = new Serializer([$normalizer], [$encoder]);
        $jsonDepartement = $serializer->serialize($departement, 'json');
        $response = new Response($jsonDepartement, 200, ["Content-Type" => "application/json"]);
        return $response;
    }

    /**
     * @Route("api/departement", name="api_post_departement", Methods="POST")
     * 
     * @SWG\Response(
     *     response=200,
     *     description="Ajoute un Département",
     *     @SWG\Schema(ref=@Model(type=Departement::class))
     *     )
     * )
     * 
     * @SWG\Parameter(
     *     name="order",
     *     in="body",
     *     @SWG\Schema(ref=@Model(type=Departement::class))
     * )
     * 
     */
    public function addDepartement(Request $request, ValidatorInterface $validator): Response
    {
        $jsonDepartement = $request->getContent();

        try {
            $departement = $this->get('serializer')->deserialize($jsonDepartement, Departement::class, 'json');
            if ($departement != null) {
                $manager = $this->getDoctrine()->getManager();

                $errors = $validator->validate($departement);
                if (count($errors) > 0) {
                    return new Response($errors, 400, ["Content-Type" => "application/json"]);
                }
                $manager->persist($departement);
                $manager->flush();
                return new Response($jsonDepartement, 201, ["Content-Type" => "application/json"]);
            }
            return $this->json(['message' => 'Impossible d\'ajouter'], 400);
        } catch (NotEncodableValueException $e) {
            return $this->json(['message' => $e->getMessage()], 400, ["Content-Type" => "application/json"]);
        }
    }

    /**
     * @Route("api/departement/{id}", name="api_put_departement", Methods="PUT")
     * 
     * @SWG\Response(
     *     response=200,
     *     description="Ajoute un departement",
     *     @SWG\Schema(ref=@Model(type=Departement::class))
     *     )
     * )
     * 
     * @SWG\Parameter(
     *     name="order",
     *     in="body",
     *     @SWG\Schema(ref=@Model(type=Departement::class))
     * )
     * 
     */
    public function updateDepartement(Request $request, $id, ValidatorInterface $validator, DepartementRepository $departementRepository): Response
    {
        $jsonDepartement = $request->getContent();

        try {
            $departement = $this->get('serializer')->deserialize($jsonDepartement, Departement::class, 'json');
            $departement_in_base = $departementRepository->find($id);

            if ($departement != null and $departement_in_base != null) {
                if ($departement->getName() != null)
                    $departement_in_base->setName($departement->getName());
                if ($departement->getCapacity() != null)
                    $departement_in_base->setCapacity($departement->getCapacity());

                $manager = $this->getDoctrine()->getManager();
                $errors = $validator->validate($departement_in_base);
                if (count($errors) > 0) {
                    return $this->json($errors, 400, ["Content-Type" => "application/json"]);
                }
                $manager->persist($departement_in_base);
                $manager->flush();
                return new Response($jsonDepartement, 201, ["Content-Type" => "application/json"]);
            }
            return $this->json(['message' => 'Impossible de modifer ce departement, vérifier bien s\'il existe ou si les données sont valide'], 400);
        } catch (NotEncodableValueException $e) {
            return $this->json(['message' => $e->getMessage()], 400, ["Content-Type" => "application/json"]);
        }
    }


    /**
     * @Route("api/departement/{id}", name="api_departement_view", Methods="GET")
     * 
     * @SWG\Response(
     *     response=200,
     *     description="Retourne un departement",
     *     @SWG\Schema(ref=@Model(type=Departement::class))
     * )
     * 
     */
    public function showDepartement(DepartementRepository $departementRepository, $id): Response
    {
        $departement =  $departementRepository->find($id);
        if ($departement != null) {

            $encoder = new JsonEncoder();
            $defaultContext = [
                AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {

                    return $object->getId();
                },
            ];
            $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

            $serializer = new Serializer([$normalizer], [$encoder]);
            $jsonDepartement = $serializer->serialize($departement, 'json');
            $response = new Response($jsonDepartement, 200, ["Content-Type" => "application/json"]);

            return $response;
        }
        return $this->json(['message' => 'Impossible d\'afficher le departement'], 400);
    }

    /**
     * @Route("api/departement/{id}", name="api_delete_departement", Methods="DELETE")
     * 
     * @SWG\Response(
     *     response=200,
     *     description="Supprime le departement",
     *     @SWG\Schema(ref=@Model(type=Departement::class))
     *     )
     * )
     * 
     */
    public function deleteDepartement($id, DepartementRepository $departementRepository): Response
    {
        $departement =  $departementRepository->find($id);
        if ($departement != null) {
            $manager = $this->getDoctrine()->getManager();
            $manager->remove($departement);
            $manager->flush();
            return $this->json(['message' => 'Departement supprimé'], 200, ["Content-Type" => "application/json"]);
        }
        return $this->json(['message' => 'Impossible de supprimer le departement'], 400);
    }



    /**
     * @Route("/api/departement/{id}/student", name="api_sudent_in_departement", Methods="GET")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Retourne Tous les étudiants dans cette departement",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Departement::class))
     *     )
     * )
     * 
     * 
     */
    public function student_in_departement($id, DepartementRepository $departementRepository, SerializerInterface $serializer)
    {
        $departement = $departementRepository->find($id);

        if ($departement != null) {
            $students = $departement->getStudents();
            if (count($students) == 0) {
                return $this->json([], 200);
            }
            $encoder = new JsonEncoder();
            $defaultContext = [
                AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {

                    return $object->getId();
                },
            ];
            $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

            $serializer = new Serializer([$normalizer], [$encoder]);
            $jsonStudent = $serializer->serialize($students, 'json');
            $response = new Response($jsonStudent, 200, ["Content-Type" => "application/json"]);
            return $response;
        }
        return $this->json(['message' => 'Erreur du département'], 400);
    }


    /**
     * @Route("/api/student_in_departement", name="api_sudent_in_all_departement", Methods="GET")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Retourne Tous les étudiants inscrits dans un departement",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Departement::class))
     *     )
     * )
     * 
     * 
     */
    public function student_in_all_departement(DepartementRepository $departementRepository, SerializerInterface $serializer)
    {
        $departements = $departementRepository->findAll();

        if ($departements != null) {
            $students = new ArrayCollection();
            foreach ($departements as $departement) {
                $studentDep = $departement->getStudents();
                if ($studentDep != null) {
                    foreach ($studentDep as $student) {
                        $students->add($student);
                    }
                }
            }
            if (count($students) == 0) {
                return $this->json([], 200);
            }
            $encoder = new JsonEncoder();
            $defaultContext = [
                AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {

                    return $object->getId();
                },
            ];
            $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

            $serializer = new Serializer([$normalizer], [$encoder]);
            $jsonStudent = $serializer->serialize($students, 'json');
            $response = new Response($jsonStudent, 200, ["Content-Type" => "application/json"]);
            return $response;
        }
        return $this->json(['message' => 'Erreur du département'], 400);
    }
}
