[![N|Solid](https://formations.univ-amu.fr/images/logo-light.png)](https://nodesource.com/products/nsolid)
# Gestionnaire Etudiant API


Il s'agit d'une application de gestion d'étudiant.  

## Auteur
 Abderemane ALLAOUI 


### Technologies

Dans ce projet, nous avons utilisé :

* symfony 4.2
* PHP 7.4.3
* Composer 2.0
* HTML5
* CSS3
* Mysql
* JavaScript
* Doctrine
* Git
* Swagger

Cette application fonctionne avec les versions supérieurs ou ceux cités . 

### Utilisation 

Configurer l'application avec votre base des données en allant dans le fichier .env

Ensuite installer les comopsant avec la commade 
```sh
$ composer install
```

Puis lancer le serveur avec la commande:
```sh
$ composer require server --dev
$ php bin/console server:run
```

Sur un autre terminale lancer les migrations avec les commandes:
```sh
$ php bin/console make:migration
$ php bin/console doctrine:migration:migrate
```

Lorsque tout est lancé, aller sur la route : 
* http://localhost:8000/ -page d'acceuil
* http://localhost:8000/api/doc - pour la documentation

* 8001 étant le port de votre serveur. 
